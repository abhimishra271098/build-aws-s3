const User = require("../models/userModel");


exports.saveObject = async (req, res) => {
  try {
    if (req.files.length !== 0) {
      const username = req.body.username;
      const filename = req.files[0].filename;
      const link = `http://localhost:3002/api/v1/users/download?filename=${filename}`;
      const newEntry = new User({ username, file: link });
      const user = await newEntry.save();

      return res.json({ message: "File uploaded successfully", data: user });
    } else {
      return res.status(400).json({ error: "No file uploaded" });
    }
  } catch (err) {
    return res.status(500).json({ message: "Internal Server Error" });
  }
};

exports.getObject = async (req, res) => {
  try {
    const existingUser = await User.findOne({ _id: req.params.userId });
    if (existingUser) {
      return res.json({
        message: "File fetch successfully",
        data: existingUser,
      });
    } else {
      return res.status(400).json({ error: "No file found" });
    }
  } catch (err) {
    return res.status(500).json({ message: "Internal Server Error" });
  }
};

exports.putObject = async (req, res) => {
  try {
    const existingUser = await User.findOne({ _id: req.params.userId });
    if (existingUser) {
      const username = req.body.username;
      const filename = req.files[0].filename;
      const link = `http://localhost:3002/api/v1/users/download?filename=${filename}`;
      existingUser.file = link;
      existingUser.username = username;
      const updatedUser = await existingUser.save();
      return res.json({
        message: "User file updated successfully",
        data: updatedUser,
      });
    } else {
      return res.status(400).json({ error: "No file updated" });
    }
  } catch (err) {
    return res.status(500).json({ message: "Internal Server Error" });
  }
};

exports.deleteObject = async (req, res) => {
  try {
    const deletedUser = await User.deleteOne({ _id: req.params.userId });
    if (deletedUser.deletedCount > 0) {
      return res.json({ message: "User file deleted successfully" });
    } else {
      return res.status(400).json({ error: "User not found" });
    }
  } catch (err) {
    return res.status(500).json({ message: "Internal Server Error" });
  }
};

exports.listObject = async (req, res) => {
  try {
    const existingUser = await User.find({});
    if (existingUser) {
      return res.json({
        message: "File fetch successfully",
        data: existingUser,
      });
    } else {
      return res.status(400).json({ error: "No file found" });
    }
  } catch (err) {
    return res.status(500).json({ message: "Internal Server Error" });
  }
};

exports.getBucket = async (req, res) => {
  try {
    const allFiles = [];
    const userDatabase = await User.find({});
    for (const entry of userDatabase) {
      const url = entry.file;
      const matches = url.match(/filename=([^&]+)/);
      if (matches && matches[1]) {
        const filename = matches[1];
        allFiles.push(`../src/uploads/${filename}`);
      } else {
        console.log("Filename not found");
      }
    }
    return res.json({ message: "Fetch file directory successfully", data: allFiles });
  } catch (err) {
    return res.status(500).json({ message: "Internal Server Error" });
  }
};
