const path = require("path");

exports.globalFileDownload = async (req, res) => {
  try {
    const file = path.join(__dirname, `../uploads/${req.query.filename}`);
    res.download(file);
  } catch (err) {
    return res.status(500).json({ message: "Internal Server Error" });
  }
};
