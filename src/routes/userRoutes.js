const express = require("express");
const router = express.Router();
const globalController = require("../controllers/globalController");
const userController = require("../controllers/userController");
const { upload } = require("../middleware/file-upload");

router.get("/download", globalController.globalFileDownload);
router.post("/save", upload.array("upload_file", 1), userController.saveObject);
router.get("/get/:userId", userController.getObject);
router.put(
  "/update/:userId",
  upload.array("upload_file", 1),
  userController.putObject
);
router.delete("/remove/:userId", userController.deleteObject);
router.get("/object/list/", userController.listObject);
router.get("/bucket", userController.getBucket);

module.exports = router;
