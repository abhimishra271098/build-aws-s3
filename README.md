# Build AWS S3
This is a simple Node.js project for managing store file in local bucket.


## Prerequisites

Make sure you have the following installed on your machine:

- Node.js (https://nodejs.org/)
- MongoDB (https://www.mongodb.com/try/download/community)

## Getting Started

1. Install dependencies:

npm install

2. Configure MongoDB:

Create a MongoDB database and obtain the connection URL.

MONGODB_URI=<your_mongodb_connection_url>

3. Run the application:

npm start

## Project Structure

The project follows the MVC (Model-View-Controller) design pattern:


blogs-project/
|-- src/
|   |-- controllers/
|   |-- models/
|   |-- routes/
|   |-- uploads/
|   |-- view/
|   |-- index.js
|-- package.json
|-- README.md


* controllers: Contains the logic for handling requests.
* models: Defines MongoDB schemas for blogs, users, and comments.
* routes: Defines routes for different endpoints.
* uploads: Define store all files.
* views: Contains views for the project (if using server-side rendering).
* index.js: Main application file.

## Dependencies

* Express: Web framework for Node.js.
* Mongoose: MongoDB object modeling for Node.js.
* multer: Uploads files.